package login;


import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.testng.Reporter;
import org.testng.annotations.Test;
import annotations.StartEngine;
import readExcel.Constants;
import readExcel.Utilities;
import resortBooking.ResortBookingAndCancellation;


public class LogIn extends StartEngine {



	 
	 public static int i = 1;
	 public static int j = 0;
	 
	 @Test(priority = 1)
	 public static void enterUserName() throws Exception {
	  Utilities.setExcelFile(Constants.Path_TestData + Constants.File_TestData, "Sheet1");
	  WebElement memberId = driver.findElement(By.xpath("//*[@id=\"username\"]"));
	  memberId.click();
	 
	  String cellData = Utilities.getCellData(i, j);
	  memberId.sendKeys(cellData);

	  Reporter.log("MemberId Entered");
	 }

	 @Test(priority = 2)
	 public static void enterPassWord() throws Exception {
	  Utilities.setExcelFile(Constants.Path_TestData + Constants.File_TestData, "Sheet1");
	  WebElement passWord = driver.findElement(By.xpath("//*[@id=\"password\"]"));
	  passWord.click();
	  j++;
	  String cellData1 = Utilities.getCellData(i, j);
	  passWord.sendKeys(cellData1);
	  Reporter.log("PasswordEntered");
	 }

	 @Test(priority = 3)
	 public void clickLogin() throws InterruptedException {
	  WebElement login = driver.findElement(By.id("login"));
	  login.click();
	  Reporter.log("LoginClicked");
	 }
	 
	 @Test(priority = 4)
	 public void handlingError() throws Exception {
	  Utilities.setExcelFile(Constants.Path_TestData + Constants.File_TestData, "Sheet1");
	  int rowCount = exut.getRowCount("Sheet1");
	  Thread.sleep(2000);
	  WebElement errorMessage = driver.findElement(By.xpath("//div[@id='error']"));
	  WebElement dashBoard = driver.findElement(By.id("resort-main-menu"));
	  for (int x = 0; x < rowCount; x++) {
	   try {
	    if (errorMessage.isDisplayed()) {
	     Reporter.log("Invalid username or password. Please try again.");
	     i++;
	     j--;
	     enterUserName();
	     enterPassWord();
	     clickLogin();
	    } else if (dashBoard.isDisplayed()) {
	    	String page_Title=driver.getTitle();
	    	System.out.println(page_Title);
	     Reporter.log("On-DashBoard");
	     break;

	    } else {
	     Reporter.log("Problem in getting data");
	    }
	   } catch (StaleElementReferenceException e) {
	    e.printStackTrace();
	   }
	  }
	 }
	}

	 



	
	 