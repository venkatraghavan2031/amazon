package annotations;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;

import readExcel.Utilities;

public class StartEngine {
	public static WebDriver driver;
	public static Utilities exut = new Utilities();
	public static JavascriptExecutor jse= (JavascriptExecutor)driver;
	 @Parameters("url")
	 @BeforeTest
	 public static void BrowserConfiguration(String url) throws Exception {
	  System.setProperty("webdriver.chrome.driver", "C:\\Users\\Venkat.Raghavan\\Desktop\\New driver\\chromedriver.exe");
	  Map < String, Object > prefs = new HashMap < String, Object > ();
	  prefs.put("profile.default_content_setting_values.notifications", 2);
      ChromeOptions options = new ChromeOptions();
	  options.setExperimentalOption("prefs", prefs);
	  driver = new ChromeDriver(options);
      driver.get(url);
	 }
}
